import {SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';
import {Provider} from 'react-redux';
import {Store} from './src/store';
import Todos from './src/container/Todos';
// import {Colors} from 'react-native/Libraries/NewAppScreen';

const style = StyleSheet.create({
  root: {
    backgroundColor: '#263238',
    flex: 1,
    flexDirection: 'column',
  },
});

const App = () => {
  // const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: '#FFFFF',
  };

  return (
    <Provider store={Store}>
      <SafeAreaView style={style.root}>
        {/* <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={backgroundStyle}> */}
        <Todos />
        {/* </ScrollView> */}
      </SafeAreaView>
    </Provider>
  );
};

export default App;
