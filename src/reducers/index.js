import {FETCH_TODO_SUCCES} from '../types';
const initialState = {
  todos: '',
};

const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TODO_SUCCES:
      return {
        ...state,
        todos: action.payload,
      };
    default:
      return state;
  }
};

export default Reducer;
