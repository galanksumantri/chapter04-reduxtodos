import {applyMiddleware, combineReducers, createStore} from 'redux';
import ReduxThunk from 'redux-thunk';
import TodosReducer from '../reducers';

const Reducers = {
  appData: TodosReducer,
};

export const Store = createStore(
  combineReducers(TodosReducer),
  applyMiddleware(ReduxThunk),
);
